Youtube link: https://www.youtube.com/watch?v=bWF8vt_uQGQ&feature=youtu.be

A side-scroller game for the Nintendo 3DS platform.
The game is still in an early stage and I am testing different features.

The player is a spacecraft that must go through a few levels of the game.
He must defeat enemies on the way, which can drop bonuses and different weapons and upgrades.
At the end of each level the player fights a boss.
There is a simple leveling system. I am planning to add RPG-like element, with talents/talent trees.

![Alt text](Screenshot_60.png?raw=true)
![Alt text](Screenshot_433.png?raw=true)