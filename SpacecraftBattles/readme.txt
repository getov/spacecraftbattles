Instructions:
Usage: cxitool [options] input.3dsx output.cxi
Options:
-n, --name=<value> Specifies the process name of the application
-c, --code=<value> Specifies the product code of the application
-t, --tid=<value> Specifies the title ID of the application
-s, --settings=<file> Specifies the settings file
-b, --banner=<file> Specifies the banner file to embed in the CXI
-v, --version Displays version information
-?, --help Displays this text

Example usage:
cxitool SpacecraftBattles.3dsx SpacecraftBattles.cxi
makerom -f cia -o SpacecraftBattles.cia -target t -i SpacecraftBattles.cxi:0:0

$DEVKITARM/bin/3dslink -a 192.168.100.3 SpacecraftBattles.3dsx

Source:
https://github.com/devkitPro/3dstools/tree/cxi-stuff
