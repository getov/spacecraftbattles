#ifndef __BOUNDING_BOX_H__
#define __BOUNDING_BOX_H__

#include "Vector.h"

class BoundingBox2D
{
public:
	// Lower and upper bounds along X, Y axes
	Vector2D min;
	Vector2D max;

	// Create an empty bounding box
	BoundingBox2D()
		: min(Vector2D())
		, max(Vector2D())
	{
	}

	// Initialize a box with a given point
	BoundingBox2D(const Vector2D& point)
		: min(point)
		, max(point)
	{
	}

	BoundingBox2D(const Vector2D& min, const Vector2D& max)
		: min(min)
		, max(max)
	{
	}

	// Set the min/max points of the box.
	void set(const Vector2D& minIn, const Vector2D& maxIn)
	{
		min = minIn;
		max = maxIn;
	}

	bool isEmpty() const
	{
		return	min.x == 0.0f && min.y == 0.0f &&
				max.x == 0.0f && max.y == 0.0f;
	}

	// Return the size along the X axis
	float sizeX() const 
	{ 
		return max.x - min.x; 
	}

	// Return the size along the X axis
	float sizeY() const 
	{ 
		return max.y - min.y; 
	}

	// Expand the box
	void operator +=(const Vector2D& p) 
	{
		min = vectorMin(min, p);
		max = vectorMax(max, p);
	}

	// Check if a point is inside the box.
	// @param p The point to check.
	// @param tolerance Optional tolerance around the box.
	// @return Returns true if the point is inside the box expanded by the tolerance, and false otherwise.
	bool isInside(const Vector2D& p, float tolerance = 0.0f) const
	{
		if (min.x - tolerance <= p.x && p.x <= max.x + tolerance &&
			min.y - tolerance <= p.y && p.y <= max.y + tolerance)
		{
			return true;
		}

		return false;
	}

	// Check if the given box is inside this one.
	// @param b The box to check.
	// @return Returns true if the given box is inside this one.
	bool contains(const BoundingBox2D& b) const
	{
		return (min[0] <= b.min[0] && b.max[0] <= max[0]) &&
			   (min[1] <= b.min[1] && b.max[1] <= max[1]);
	}

	// Check if the given box intersects this one
	// @param b The box to check.
	// @param tolerance Optional tolerance around the box.
	// @return Returns true if the given box overlaps this one expanded by the tolerance.
	bool intersect(const BoundingBox2D& b, float tolerance) const
	{
		if (max.x + tolerance<b.min.x || min.x - tolerance>b.max.x)
		{
			return false;
		}

		if (max.y + tolerance<b.min.y || min.y - tolerance>b.max.y)
		{
			return false;
		}

		return true;
	}

	// Check if the given box intersects this one
	// @param b The box to check.
	// @param tolerance Optional tolerance around the box.
	// @return Returns true if the given box overlaps this one expanded by the tolerance.
	bool intersect(const BoundingBox2D& b) const
	{
		bool result = true;

		// This for Pentium4 processors is compiled to "conditional move" instructions
		if (max.x < b.min.x)
		{
			result = false;
		}

		if (min.x > b.max.x)
		{
			result = false;
		}

		if (max.y < b.min.y)
		{
			result = false;
		}

		if (min.y > b.max.y)
		{
			result = false;
		}

		return result;
	}
};

#endif