#include "Enemies.h"
#include "Player.h"
#include "system.h"
#include <cmath>

SimpleEnemy::SimpleEnemy()
{
}

SimpleEnemy::SimpleEnemy(const Vector2D& pos, const BoundingBox2D& bBox, float speed, int spriteIndex)
{
	init(pos, bBox, speed, spriteIndex);
}

SimpleEnemy::SimpleEnemy(const SimpleEnemy& other)
{
	*this = other;
}

SimpleEnemy& SimpleEnemy::operator=(const SimpleEnemy& other)
{
	*this = other;
	return *this;
}

void SimpleEnemy::init(const Vector2D& pos, const BoundingBox2D& bBox, float speed, int spriteIndex)
{
	type = GameObjectType::enemy;
	position = pos;
	boundingBox = bBox;
	imageIndex = spriteIndex;
	velocity = speed;
	isAlive = true;
	shouldFire = true;
	projectile.init(pos,
					BoundingBox2D(Vector2D(0.0f, 0.0f), Vector2D(16.0f, 28.0f)),
					400.0f,
					2);

	tickProjectileFired = 0;
	health = 100;
	damage = 50;
	tileSize = Vector2D(22.0f, 28.0f);
}

bool SimpleEnemy::isCollidingWith(const GameObject& object)
{
	return boundingBox.intersect(object.boundingBox);
}

SimpleEnemy::~SimpleEnemy()
{
}

void SimpleEnemy::setPosition(const Vector2D& pos)
{
	position = pos;
	boundingBox.min = position;
	boundingBox.max = position + tileSize;
}

void SimpleEnemy::updatePosition(const Vector2D& offset, float deltaTime)
{
	position += const_cast<Vector2D&>(offset)*deltaTime*velocity;
	boundingBox.min = position;
	boundingBox.max = position + tileSize;
}

void SimpleEnemy::setImageIndex(int index)
{
	imageIndex = index;
}

int SimpleEnemy::getImageIndex()
{
	return imageIndex;
}

void SimpleEnemy::setVelocity(float speed)
{
	velocity = speed;
}

float SimpleEnemy::getVelocity() const
{
	return velocity;
}

int SimpleEnemy::getHP() const
{
	return health;
}

void SimpleEnemy::setHP(int hp)
{
	health = hp;
}

int SimpleEnemy::getDMG() const
{
	return damage;
}

void SimpleEnemy::setDMG(int dmg)
{
	damage = dmg;
}

const Vector2D& SimpleEnemy::getTileSize() const
{
	return tileSize;
}


// First Boss

FirstBossTimers::FirstBossTimers()
	: lastUpdateTimeMotion(0.0f)
	, lastUpdatetimeDustWeapon(0.0f)
	, lastUpdatetimeDustWeaponVisible(0.0f)
	, spawnTime(0.0f)
{
}

FirstBossTimers::~FirstBossTimers()
{
}

float FirstBossTimers::timeDiffLastUpdateTimeMotion(float currTime)
{
	return currTime - lastUpdateTimeMotion;
}

float FirstBossTimers::timeDiffLastUpdatetimeDustWeapon(float currTime)
{
	return currTime - lastUpdatetimeDustWeapon;
}

float FirstBossTimers::timeDiffLastUpdateTimeDustVisible(float currTime)
{
	return currTime - lastUpdatetimeDustWeaponVisible;
}

FirstBoss::FirstBoss()
{
}

FirstBoss::FirstBoss(const Vector2D& pos, float speed, int spriteIndex)
{
	init(pos, speed, spriteIndex);
}

FirstBoss::~FirstBoss()
{
	if (timer)
	{
		delete timer;
	}
}

void FirstBoss::init(const Vector2D& pos, float speed, int spriteIndex)
{
	type = GameObjectType::enemy; // maybe add a boss object type
	position = pos;
	imageIndex = spriteIndex;
	velocity = speed;
	isAlive = false;
	isSpawned = false;

	maxHealth = 2000;
	health = maxHealth;
	damage = 120;
	tileSize = Vector2D(88.0f, 112.0f);
	boundingBox = BoundingBox2D(position, position + tileSize);
	updateDirection = 0.0f;
	oldPlayerPos = 0.0f;

	fireDust = false;
	dustWeapon.reserve(30);
	for (int i = 0; i < 30; ++i)
	{
		dustWeapon.push_back(DustWeapon(false,
							 Vector2D(10.0f, 10.0f)));
		
	}

	timer = new FirstBossTimers;
	isFireDustVisible = false;
}

void FirstBoss::spawn(float delta)
{
	isAlive = true;

	// move the boss to its starting position
	if (position.x > 300.0f)
	{
		updatePosition(Vector2D(-1.0f, 0.0f), delta);
	}
	else
	{
		isSpawned = true;
		timer->spawnTime = getSystemTimeSec();
		timer->lastUpdateTimeMotion = timer->spawnTime;
	}
}

void FirstBoss::executePhaseOne(PlayerSpaceCraft& player, float delta, float currTime)
{
}

void FirstBoss::executePhaseTwo(PlayerSpaceCraft& player, float delta, float currTime)
{
}

void FirstBoss::executePhaseThree(PlayerSpaceCraft& player, float delta, float currTime)
{
	if (isSpawned)
	{
		if (timer->timeDiffLastUpdateTimeDustVisible(currTime) > 2.5f)
		{
			// make the fireDust visible
			isFireDustVisible = true;
			timer->lastUpdatetimeDustWeaponVisible = currTime;
		}

		if (timer->timeDiffLastUpdatetimeDustWeapon(currTime) > 3.5f)
		{
			fireDust = true;
			timer->lastUpdatetimeDustWeapon = currTime;

			// TODO: set this elsewhere
			isFireDustVisible = false;
		}
		else
		{
			fireDust = false;
		}
	}

	// use weapon
	for (int i = 0; i < dustWeapon.size(); ++i)
	{
		float vel = 200.0f;
		if (i < 10)
		{
			if (fireDust)
			{
				dustWeapon[i].projectile.min = position + Vector2D(0.0f + i, 10.0f);
				dustWeapon[i].isVisible = true;
			}

			if (dustWeapon[i].isVisible)
			{
				vel = vel*(i + 1)*0.5f;
				dustWeapon[i].projectile.min += Vector2D(-2.5f, -2.5f)*delta*vel;
			}
		}
		else if (i > 9 && i < 20)
		{
			if (fireDust)
			{
				dustWeapon[i].projectile.min = position + Vector2D(5.0f + i - 9, 55.0f);
				dustWeapon[i].isVisible = true;
			}

			if (dustWeapon[i].isVisible)
			{
				vel = vel*(i - 8)*0.5f;
				dustWeapon[i].projectile.min += Vector2D(-1.0f, 0.0f)*delta*vel;
			}
		}
		else
		{
			if (fireDust)
			{
				dustWeapon[i].projectile.min = position + Vector2D(0.0f + i - 20, 85.0f);
				dustWeapon[i].isVisible = true;
			}

			if (dustWeapon[i].isVisible)
			{
				vel = vel*(i - 19)*0.5f;
				dustWeapon[i].projectile.min += Vector2D(-1.5f, 1.5f)*delta*vel;
			}
		}

		// monitor screen
		if (dustWeapon[i].isVisible)
		{
			dustWeapon[i].projectile.max = dustWeapon[i].projectile.min + dustWeapon[i].size;

			if (!dustWeapon[i].projectile.intersect(BoundingBox2D(Vector2D(0.0f, 0.0f), Vector2D(400, 240))))
			{
				dustWeapon[i].isVisible = false;
				continue;
			}

			if (player.isAlive && player.boundingBox.intersect(dustWeapon[i].projectile))
			{
				player.takeDamage(getDMG());
				dustWeapon[i].isVisible = false;
			}
		}
	}
}

void FirstBoss::executeScript(PlayerSpaceCraft& player, float delta)
{
	const float currentTime = getSystemTimeSec();

	// TODO: Enter in phase 1
	if (false)
	{
		executePhaseOne(player, delta, currentTime);
	}
	// TODO: Enter in phase 2
	if (false)
	{
		executePhaseTwo(player, delta, currentTime);
	}

	// Enter in phase 3 when the boss reaches 20% health
	if (/*health < 0.2*maxHealth*/true) // execute always for now
	{
		executePhaseThree(player, delta, currentTime);
	}

	// do this each 1.5 seconds
	if (timer->timeDiffLastUpdateTimeMotion(currentTime) > 1.5f)
	{
		oldPlayerPos = player.position.y - 30.0f;
		const float positionDiff = oldPlayerPos - position.y;

		// kinda follow player's Y direction
		if (positionDiff < 0.0f)
		{
			updateDirection = -1.0f;
		}
		else if (positionDiff > 0.0f)
		{
			updateDirection = 1.0f;
		}
		timer->lastUpdateTimeMotion = currentTime;
	}

	updatePosition(Vector2D(0.0f, updateDirection), delta);
	if (abs(oldPlayerPos - position.y) < 2.0)
	{
		position.y = oldPlayerPos;
		boundingBox.min = position;
		boundingBox.max = position + getTileSize();
	}
}

int FirstBoss::getHP() const
{
	return health;
}

void FirstBoss::setHP(int hp)
{
	health = hp;
}

int FirstBoss::getMaxHP() const
{
	return maxHealth;
}

int FirstBoss::getDMG() const
{
	return damage;
}

void FirstBoss::setDMG(int dmg)
{
	damage = dmg;
}

void FirstBoss::takeDamage(int dmg)
{
	// while the health is over 90%, take half decreased damage
	if (health > 0.9*maxHealth)
	{
		dmg *= 0.5;
	}

	health -= dmg;

	if (health <= 0)
	{
		health = 0;
		isAlive = false;
		isSpawned = false;
	}
}

bool FirstBoss::isCollidingWith(const GameObject& object)
{
	return boundingBox.intersect(object.boundingBox);
}

void FirstBoss::setPosition(const Vector2D& pos)
{
	position = pos;
	boundingBox.min = position;
	boundingBox.max = position + tileSize;
}

void FirstBoss::updatePosition(const Vector2D& offset, float deltaTime)
{
	position += const_cast<Vector2D&>(offset)*deltaTime*velocity;
	boundingBox.min = position;
	boundingBox.max = position + tileSize;
}

int FirstBoss::getImageIndex()
{
	return imageIndex;
}

const Vector2D& FirstBoss::getTileSize() const
{
	return tileSize;
}