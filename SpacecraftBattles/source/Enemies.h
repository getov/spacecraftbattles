#ifndef __ENEMIES_H__
#define __ENEMIES_H__

#include "GameObject.h"
#include "Weapons.h"
#include <vector>

class PlayerSpaceCraft;

class SimpleEnemy : public GameObject
{
public:
	// A flag to indicate if the SpaceCraft is alive
	bool isAlive;

	Projectile projectile;
	bool shouldFire;
	unsigned long long tickProjectileFired;

	SimpleEnemy();

	SimpleEnemy(const SimpleEnemy& other);
	SimpleEnemy(SimpleEnemy&& other) = default;
	SimpleEnemy& operator=(const SimpleEnemy& other);

	SimpleEnemy(const Vector2D& pos, const BoundingBox2D& bBox, float speed, int spriteIndex);

	~SimpleEnemy();

	void init(const Vector2D& pos, const BoundingBox2D& bBox, float speed, int spriteIndex);

	// From GameObject
	// Check for collisons with another game object
	virtual bool isCollidingWith(const GameObject& object) override;

	// Set the position and update the bounding box
	virtual void setPosition(const Vector2D& pos) override;

	// Update the position by adding an offset to the current position
	// Update the bounding box too.
	virtual void updatePosition(const Vector2D& offset, float deltaTime) override;

	void setImageIndex(int index);

	int getImageIndex();

	void setVelocity(float speed);

	float getVelocity() const;

	// Return the Enemy's HP
	int getHP() const;

	// Set the Enemy's HP
	void setHP(int hp);

	int getDMG() const;

	void setDMG(int dmg);

	const Vector2D& getTileSize() const;

private:
	// index of the image in the sprite
	int imageIndex;

	Vector2D tileSize;

	// the movement speed of the SpaceCraft
	float velocity;

	// the health of the enemy
	int health;

	// damage
	int damage;
};

// First boss of the game

//temp weapon
struct DustWeapon
{
	DustWeapon(bool visible, const Vector2D& vsize)
	{
		isVisible = visible;
		size = vsize;
		projectile = BoundingBox2D(Vector2D(0.0f, 0.0f), vsize);
	}

	bool isVisible;
	BoundingBox2D projectile;
	Vector2D size;
};

struct FirstBossTimers
{
	FirstBossTimers();
	~FirstBossTimers();

	// Returns the difference between the current time and lastUpdateTimeMotion
	float timeDiffLastUpdateTimeMotion(float currTime);

	// Returns the difference between the current time and lastUpdatetimeDustWeapon
	float timeDiffLastUpdatetimeDustWeapon(float currTime);

	float timeDiffLastUpdateTimeDustVisible(float currTime);

	float lastUpdateTimeMotion;
	float lastUpdatetimeDustWeapon;
	float lastUpdatetimeDustWeaponVisible;
	float spawnTime;
};

class FirstBoss : public GameObject
{
public:
	bool isAlive;

	FirstBoss();

	FirstBoss(const Vector2D& pos, float speed, int spriteIndex);

	~FirstBoss();

	void init(const Vector2D& pos, float speed, int spriteIndex);

	void spawn(float delta);

	// execute the boss fight logic
	void executeScript(PlayerSpaceCraft& player, float delta);

	// From GameObject
	// Check for collisons with another game object
	virtual bool isCollidingWith(const GameObject& object) override;

	// Set the position and update the bounding box
	virtual void setPosition(const Vector2D& pos) override;

	// Update the position by adding an offset to the current position
	// Update the bounding box too.
	virtual void updatePosition(const Vector2D& offset, float deltaTime) override;

	int getImageIndex();

	const Vector2D& getTileSize() const;

	int getHP() const;

	void setHP(int hp);

	int getMaxHP() const;

	int getDMG() const;

	void setDMG(int dmg);

	void takeDamage(int dmg);

	// test particle weapon
	std::vector<DustWeapon> dustWeapon;
	bool isSpawned;
	bool isFireDustVisible;
	
private:
	// index of the image in the sprite
	int imageIndex;

	Vector2D tileSize;

	// the movement speed of the SpaceCraft
	float velocity;

	// the health of the enemy
	int health;

	int maxHealth;

	// damage
	int damage;

	FirstBossTimers* timer;

	float updateDirection;
	float oldPlayerPos;

	bool fireDust;

	void executePhaseOne(PlayerSpaceCraft& player, float delta, float currTime);
	void executePhaseTwo(PlayerSpaceCraft& player, float delta, float currTime);
	void executePhaseThree(PlayerSpaceCraft& player, float delta, float currTime);
};

#endif