#ifndef __GAME_APP_H__
#define __GAME_APP_H__

#include "Player.h"
#include "Weapons.h"
#include <vector>

#ifdef __BUILD_WINDOWS__
class OGL_GraphicsEngine;
#else
class N3DS_GraphicsEngine;
#endif

class SimpleEnemy;
class FirstBoss;
class LootManager;

class GameApp
{
public:
	static GameApp& getInstance();
	int run();

private:
#ifdef __BUILD_WINDOWS__
	OGL_GraphicsEngine* graphicsEngine;
#else
	N3DS_GraphicsEngine* graphicsEngine;
#endif

	PlayerSpaceCraft playerOneSpaceCraft;
	std::vector<SimpleEnemy> enemies;
	FirstBoss* firstBoss; // to do something about grouping enemies and bosses

	// TODO: this must be part of the Player's class
	int projectileIndex;
	std::vector<Projectile> projectileArray;

	int framesCount;
	int lastUpdateFramesCount;

	BoundingBox2D monitorScreen;

	// the X position of the two backgrounds
	float backgroundPosX[2];

	// temp test
	int enemiesDead;
	int killCount;

	// Loot manager to handle drop and loot systems
	LootManager* lootManager;

	GameApp();

	// This is never called.
	// Manually use GameApp::Destroy() instead.
	~GameApp();

	// Free the allocated resources
	void Destroy();

	// Initialize the main components of the game
	void initialize();

	// Render a single frame of the game
	// @param iod - Interocular distance
	void renderFrame(float startTime, float iod);

	// Per-frame updating of the components of the game
	void update(float deltaTime);

	void updateBackground(float deltaTime);

	Projectile& getNextProjectile();

	void displayFPSCounter(float deltaTime);

	void updateEnemies(float deltaTime);

	// Don't allow multiple instances of this class
	GameApp(const GameApp&) = delete;
	GameApp& operator=(const GameApp&) = delete;
};

#endif