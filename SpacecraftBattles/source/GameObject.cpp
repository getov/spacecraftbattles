#include "GameObject.h"

GameObject::GameObject()
{
}

GameObject::~GameObject()
{
}

GameObject::GameObject(GameObjectType objType, const Vector2D& pos, const BoundingBox2D& bBox)
	: position(pos)
	, boundingBox(bBox)
	, type(objType)
{
}

GameObjectType GameObject::getType() const
{
	return type;
}