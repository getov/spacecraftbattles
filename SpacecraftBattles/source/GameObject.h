#ifndef __GAME_OBJECT_H__
#define __GAME_OBJECT_H__

#include "BoundingBox.h"

enum class GameObjectType
{
	player,
	player2, // reserved for future use
	enemy,
	projectile,
	loot,
};

// A class representing any interactive object in the game
class GameObject
{
public:
	Vector2D position;
	BoundingBox2D boundingBox;

	GameObject();

	GameObject(GameObjectType objType, const Vector2D& pos, const BoundingBox2D& bBox);

	virtual ~GameObject();

	// Return the type of the game object
	GameObjectType getType() const;

	// Set the position and update the bounding box
	virtual void setPosition(const Vector2D& pos) = 0;

	// Update the position by adding an offset to the current position
	// Update the bounding box too.
	virtual void updatePosition(const Vector2D& offset, float deltaTime) = 0;

	// Check for collision with another game object
	virtual bool isCollidingWith(const GameObject& object) = 0;

protected:
	GameObjectType type;

private:

};

#endif