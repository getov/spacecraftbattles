#ifndef __IGRAPHICS_ENGINE__
#define __IGRAPHICS_ENGINE__

struct SpriteIndices
{
	float left, right, top, bottom;
};

// Initialized in RawData.cpp
extern SpriteIndices mainSpriteSheetTexCoords[257];

enum class RenderTargetType
{
	gfx_right = 0,
	gfx_left,
};

class IGraphicsEngine
{
public:
	// Do all initializations of the Graphics engine here
	// @return - error code
	virtual int init() = 0;

	// Set texture environment options
	// See https://www.opengl.org/sdk/docs/man2/xhtml/glTexEnv.xml
	virtual void setTexEnv() = 0;

	// Enable or disable depth testing
	virtual void enableDepthTest(bool test) = 0;
};

#endif