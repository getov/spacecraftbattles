/*
* WARNING: This Loot Manager was written overnight and it is just an idea for implementing a loot system.
* It will probably be changed in the near future because it has flaws.
* Loot table for each enemy needs to be implemented.
*/

#include "LootManager.h"
#include "Player.h"

BonusBuff::BonusBuff(LootType lootType)
{
	init(buffType);
}

BonusBuff::~BonusBuff()
{
}

void BonusBuff::init(LootType lootType)
{
	type = GameObjectType::loot;
	tileSize = Vector2D(32.0f, 32.0f);
	position = Vector2D(0.0f, 0.0f);
	imageIndex = 4;
	boundingBox.min = position;
	boundingBox.max = position + tileSize;
	buffType = lootType;
	isVisible = false;
}

void BonusBuff::setPosition(const Vector2D& pos)
{
	position = pos;
}

void BonusBuff::updatePosition(const Vector2D& offset, float deltaTime)
{
	position += const_cast<Vector2D&>(offset)*deltaTime*60.0f;
	boundingBox.min = position;
	boundingBox.max = position + tileSize;
}

bool BonusBuff::isCollidingWith(const GameObject& object)
{
	return boundingBox.intersect(object.boundingBox);
}

int BonusBuff::getImageIndex()
{
	return imageIndex;
}

const Vector2D& BonusBuff::getTileSize() const
{
	return tileSize;
}

LootType BonusBuff::getLootType() const
{
	return buffType;
}

LootManager::LootManager()
{
	lootObjects.reserve(MAX_LOOTS_AT_TIME);
	for (int i = 0; i < MAX_LOOTS_AT_TIME; ++i)
	{
		lootObjects.push_back(new BonusBuff(LootType::shieldHP));
	}
}

LootManager::~LootManager()
{
	for (auto i = lootObjects.begin(); i != lootObjects.end(); ++i)
	{
		delete *i;
	}
}

void LootManager::maybeDropLoot(GameObjectType enemyType, const Vector2D pos)
{
	// I have just one enemy type atm
	switch (enemyType)
	{
		case GameObjectType::enemy:
		{
			// do something for "simple enemy" in order to calculate the drop chance
			// ........
			bool shouldDrop = true;

			if (shouldDrop)
			{
				for (auto loot = lootObjects.begin(); loot != lootObjects.end(); ++loot)
				{
					if ((*loot)->getType() == GameObjectType::loot)
					{
						BonusBuff* buff = static_cast<BonusBuff*>(*loot);
						// we want to do this if this buff is not active
						if (!buff->isVisible)
						{
							buff->isVisible = true;
							buff->setPosition(pos);
							//switch (buff->getLootType())
							//{
							//	case LootType::shieldHP:
							//	{
							//		player->increaseShieldHP(50);
							//		break;
							//	}
							//}
							// we don't want to loop through the rest of the array
							break;
						}
					}
				}
			}
			break;
		}
	}
}

void LootManager::applyLootToPlayer(PlayerSpaceCraft* player, float deltaTime)
{
	for (auto loot = lootObjects.begin(); loot != lootObjects.end(); ++loot)
	{
		if ((*loot)->getType() == GameObjectType::loot)
		{
			BonusBuff* buff = static_cast<BonusBuff*>(*loot);
			buff->updatePosition(Vector2D(-1.0f, 0.0f), deltaTime);
			if (buff->position.x < -buff->getTileSize().x)
			{
				buff->isVisible = false;
			}

			if (buff->isVisible)
			{
				if (buff->isCollidingWith(*player))
				{
					player->increaseShieldHP(50);
					buff->isVisible = false;
				}
			}
		}
	}
}

const std::vector<GameObject*>& LootManager::getLootObjects() const
{
	return lootObjects;
}