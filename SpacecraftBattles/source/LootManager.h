#ifndef __LOOT_MANAGER_H__
#define __LOOT_MANAGER_H__

/*
* WARNING: This Loot Manager was written overnight and it is just an idea for implementing a loot system.
* It will probably be changed in the near future because it has flaws.
* Loot table for each enemy needs to be implemented.
*/

#include "GameObject.h"
#include <vector>
class Vector2D;
class PlayerSpaceCraft;

const int MAX_LOOTS_AT_TIME = 2;

// Type of the looted object
enum class LootType
{
	shieldHP = 0,
};

class BonusBuff : public GameObject
{
public:
	bool isVisible;

	explicit BonusBuff(LootType lootType);
	~BonusBuff();

	void init(LootType lootType);

	// Set the position and update the bounding box
	virtual void setPosition(const Vector2D& pos) override;

	// Update the position by adding an offset to the current position
	// Update the bounding box too.
	virtual void updatePosition(const Vector2D& offset, float deltaTime) override;

	// Check for collision with another game object
	virtual bool isCollidingWith(const GameObject& object) override;

	int getImageIndex();
	const Vector2D& getTileSize() const;

	LootType getLootType() const;

private:
	int imageIndex;

	Vector2D tileSize;

	LootType buffType;
};

class LootManager
{
public:
	LootManager();
	~LootManager();

	// Calculate a probability to drop loot based on the enemy type
	// @enemyType - the enemy from which the loot dropped
	// @pos - the position at which the loot should be spawned
	void maybeDropLoot(GameObjectType enemyType, const Vector2D pos);

	// Update the loot object position and check for a collision with the player
	// If it collides add the bonus to the player and mark the object as invisible
	void applyLootToPlayer(PlayerSpaceCraft* player, float deltaTime);

	const std::vector<GameObject*>& getLootObjects() const;
private:
	// Predefined array of loot objects
	std::vector<GameObject*> lootObjects;
};

#endif