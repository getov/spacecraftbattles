#ifndef __BUILD_WINDOWS__

#include "N3DS_GPUProgram.h"

N3DS_GPUProgram::N3DS_GPUProgram()
	: vshaderSource(nullptr)
	, attributeInfo(nullptr)
	, colorOutputMode(GPU_TEVSRC::GPU_TEXTURE0)
{
}

N3DS_GPUProgram::~N3DS_GPUProgram()
{
	// Free the shader program
	shaderProgramFree(&program);
	DVLB_Free(vshaderSource);
}

void N3DS_GPUProgram::init(u32* shbinData, u32 shbinSize, int numShaderUniforms, GPU_TEVSRC source, int attributeMode)
{
	colorOutputMode = source;

	// Load the vertex shader and create a shader program
	vshaderSource = DVLB_ParseFile(shbinData, shbinSize);

	// the API is not well documented, so from my testing I found out that if:
	// res = 0  - the program has been inited successfully
	// res = -1 - the program has failed to init
	int res = shaderProgramInit(&program);
	shaderProgramSetVsh(&program, &vshaderSource->DVLE[0]);

	C3D_BindProgram(&program);

	// Configure attributes for use with the vertex shader
	// Attribute format and element count are ignored in immediate mode

	// for now using these presets, until I implement a better attribute handling system
	if (attributeMode == 0)
	{
		attributeInfo = C3D_GetAttrInfo();
		AttrInfo_Init(attributeInfo);
		AttrInfo_AddLoader(attributeInfo, 0, GPU_FLOAT, 3); // v0=position
		AttrInfo_AddLoader(attributeInfo, 1, GPU_FLOAT, 2); // v2=texcoord
	}
	else
	{
		attributeInfo = C3D_GetAttrInfo();
		AttrInfo_Init(attributeInfo);
		AttrInfo_AddLoader(attributeInfo, 0, GPU_FLOAT, 3); // v0=position
		AttrInfo_AddLoader(attributeInfo, 1, GPU_FLOAT, 3); // v2=color
	}

	// Configure buffers
	C3D_BufInfo* bufInfo = C3D_GetBufInfo();
	BufInfo_Init(bufInfo);

	uniformsLocation.resize(numShaderUniforms, -1);
	for (int i = 0; i < numShaderUniforms; ++i)
	{
		uniformsLocation[i] = shaderInstanceGetUniformLocation(program.vertexShader, uniformsNames[i]);
	}

	setColorOutputMode(source);
}

void N3DS_GPUProgram::setColorOutputMode(GPU_TEVSRC source)
{
	// See https://www.opengl.org/sdk/docs/man2/xhtml/glTexEnv.xml for more insight
	C3D_TexEnv* env = C3D_GetTexEnv(0);
	C3D_TexEnvSrc(env, C3D_Both, source, 0, 0);
	C3D_TexEnvOp(env, C3D_Both, 0, 0, 0);
	C3D_TexEnvFunc(env, C3D_Both, GPU_REPLACE);
}

int N3DS_GPUProgram::getUniformLocation(UniformsType uniform) const
{
	return uniformsLocation[uniform];
}

void N3DS_GPUProgram::use()
{
	C3D_BindProgram(&program);
	C3D_SetAttrInfo(attributeInfo);
	setColorOutputMode(colorOutputMode);
}

#endif // __BUILD_WINDOWS__