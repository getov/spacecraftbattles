#ifndef __BUILD_WINDOWS__

#ifndef __N3DS_GPUPROGRAM_H__
#define __N3DS_GPUPROGRAM_H__

#include <3ds.h>
#include <citro3d.h>
#include <vector>

// The values of this enum are used as indices of the N3DS_GPUProgram::uniformsLocation vector and uniformsNames[]
enum UniformsType
{
	projection = 0,
};

// pre-filled array with names of uniforms
static char* uniformsNames[] = {
	"projection",
};

class N3DS_GPUProgram
{
public:
	N3DS_GPUProgram();
	~N3DS_GPUProgram();

	void init(u32* shbinData, u32 shbinSize, int numShaderUniforms, GPU_TEVSRC source, int attributeMode);

	void setColorOutputMode(GPU_TEVSRC source);

	int getUniformLocation(UniformsType uniform) const;

	// Bind the shader for using
	// This should be called before updating uniforms and drawing primitives
	void use();

private:
	// describes an instance of a full shader program
	// WARNING: If this is a pointer and I pass it to shaderProgramInit(), it blows up.
	// it looks like the API doesn't do checks for nullptr and doesn't do any initialization of the structure
	// but it writes directly to it.
	shaderProgram_s program;

	// shader data
	DVLB_s* vshaderSource;

	// A structure with the description of the shader's attributes
	C3D_AttrInfo* attributeInfo;

	GPU_TEVSRC colorOutputMode;

	// vector of the locations of the uniforms
	std::vector<int> uniformsLocation;
};

#endif

#endif // __BUILD_WINDOWS__