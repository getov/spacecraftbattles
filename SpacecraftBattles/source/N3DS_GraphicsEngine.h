#ifndef __BUILD_WINDOWS__

#ifndef __N3DS_GRAPHICS_ENGINE_H__
#define __N3DS_GRAPHICS_ENGINE_H__

#include "IGraphicsEngine.h"
#include "Sprite.h"
#include <3ds.h>
#include <citro3d.h>
#include <vector>

class N3DS_GPUProgram;
class PlayerSpaceCraft;
class FirstBoss;

const int NUM_GPU_PROGRAMS = 2;

class N3DS_GraphicsEngine : public IGraphicsEngine
{
public:
	N3DS_GraphicsEngine();
	virtual ~N3DS_GraphicsEngine();

	// Do all initializations of the Graphics engine here
	// @return - error code
	virtual int init() override;

	// Set texture environment options
	// See https://www.opengl.org/sdk/docs/man2/xhtml/glTexEnv.xml
	virtual void setTexEnv() override;

	// Enable or disable depth testing
	virtual void enableDepthTest(bool test) override;

	// Call to C3D_FrameBegin(C3D_FRAME_SYNCDRAW)
	void beginFrame();

	// Call to C3D_FrameEnd(0)
	void endFrame();

	// Set the render target on which we should render
	void setRenderTarget(RenderTargetType target);

	void drawSprite(float x, float y, float width, float height, int image);
	void drawTriangle(float x, float y, float width, float height);
	void drawRectangle(float x, float y, float width, float height, float* color, float alpha = 1.0f);
	
	// Method for drawing the HUD elements
	void drawHUD(const PlayerSpaceCraft& player);
	void drawBossHUD(const FirstBoss& boss);

	void bindTexture(C3D_Tex* texture);

	// Draw the animated background
	// @param backgroundPos - pointer to a 2D array with the X positions for background 1 and 2
	void drawBackground(float* backgroundPos);

	void updateUniforms(float iod);

	// Adjust the ortho projection matrix, depending on the Interocular distance parameter (iod)
	void orthoProjectionAdjust(float iod);

	N3DS_GPUProgram* getShaderProgram(int index);

	Sprite mainSpriteSheet;
	Sprite background;
	Sprite backgroundLayerOne;

private:
	C3D_RenderTarget* renderTargetGfxLeft;
	C3D_RenderTarget* renderTargetGfxRight;
	//N3DS_GPUProgram* shaderProgram;
	std::vector<N3DS_GPUProgram*> shaderPrograms;
	C3D_Mtx projection;

	void initShaderPrograms();
};

#endif

#endif // __BUILD_WINDOWS__