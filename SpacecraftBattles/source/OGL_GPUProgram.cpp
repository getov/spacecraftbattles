#ifdef __BUILD_WINDOWS__

#include "OGL_GPUProgram.h"
#include <iostream>
#include <fstream>
#include <sstream>

OGL_GPUProgram::OGL_GPUProgram()
	: glObject(0)
{
	glObject = glCreateProgram();
}

OGL_GPUProgram::~OGL_GPUProgram()
{
	glDeleteProgram(glObject);
}

void OGL_GPUProgram::use()
{
	glUseProgram(glObject);
}

int OGL_GPUProgram::addShader(GLenum shaderType, const char* name)
{
	std::ifstream f;
	f.open(name, std::ios::in | std::ios::binary);
	std::stringstream buffer;
	buffer << f.rdbuf();
	f.close();

	GLuint shader = glCreateShader(shaderType);
	if (shader == GL_FALSE)
	{
		std::cerr << "glCreateShader failed\n";
		return 0;
	}

	std::string tmp(buffer.str());
	const char* source = tmp.c_str();
	glShaderSource(shader, 1, (const GLchar**)&(source), NULL);

	glCompileShader(shader);
	GLint status;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE)
	{
		std::cerr << "glCompileShader failed\n";
		glDeleteShader(shader);
		return status;
	}

	shaders.push_back(shader);
	return status;
}

int OGL_GPUProgram::link()
{
	for (size_t i = 0; i < shaders.size(); ++i)
	{
		glAttachShader(glObject, shaders[i]);
	}

	glLinkProgram(glObject);

	// detach and delete the shaders
	for (size_t i = 0; i < shaders.size(); ++i)
	{
		glDetachShader(glObject, shaders[i]);
		glDeleteShader(shaders[i]);
	}

	GLint status;
	glGetProgramiv(glObject, GL_LINK_STATUS, &status);
	if (status == GL_FALSE)
	{
		std::cerr << "glLinkProgram failed\n";
	}

	return status;
}

int OGL_GPUProgram::createShaderProgram(const char* vertexShader, const char* fragmentShader, const char* geometryShader)
{
	if (!vertexShader || !fragmentShader)
	{
		return 0;
	}

	if (!addShader(GL_VERTEX_SHADER, vertexShader))
	{
		return 0;
	}

	if (!addShader(GL_FRAGMENT_SHADER, fragmentShader))
	{
		return 0;
	}

	if (geometryShader)
	{
		if (!addShader(GL_GEOMETRY_SHADER, geometryShader))
		{
			return 0;
		}
	}

	if (!link())
	{
		return 0;
	}

	return 1;
}

int OGL_GPUProgram::getAttribLocation(const char* name) const
{
	GLint status = -1;
	if (name)
	{
		status = glGetAttribLocation(glObject, name);
		if (status == -1)
		{
			std::cerr << "Program attribute not found: " << name << std::endl;
		}
	}
	else
	{
		std::cerr << "attribName is NULL\n";
	}

	return status;
}

GLint OGL_GPUProgram::getUniform(const char* name) const
{
	GLint status = -1;
	if (name)
	{
		status = glGetUniformLocation(glObject, name);
		if (status == -1)
		{
			std::cerr << "Program uniform not found: " << name << std::endl;
		}
	}
	else
	{
		std::cerr << "uniform is NULL\n";
	}

	return status;
}

#endif // __BUILD_WINDOWS__