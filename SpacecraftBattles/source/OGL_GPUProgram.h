#ifdef __BUILD_WINDOWS__

#ifndef __OGL_GPUPROGRAM_H__
#define __OGL_GPUPROGRAM_H__

#include <GL\glew.h>
#include <GL\glfw3.h>
#include <vector>

class OGL_GPUProgram
{
public:
	OGL_GPUProgram();
	~OGL_GPUProgram();

	// Bind the shader for using
	// This should be called before updating uniforms and drawing primitives
	void use();

	// Creates a shader program with given vertes, fragment and optionally geometry shader
	int createShaderProgram(const char* vertexShader, const char* fragmentShader, const char* geometryShader = nullptr);

	GLint getAttribLocation(const char* name) const;
	//void SetUniform(const GLchar* name, const glm::mat2& matrix, GLboolean transpose = GL_FALSE);
	//void SetUniform(const GLchar* name, const glm::mat3& matrix, GLboolean transpose = GL_FALSE);
	//void SetUniform(const GLchar* name, const glm::mat4& matrix, GLboolean transpose = GL_FALSE);
	//void SetUniform(const GLchar* name, const glm::vec3& v);
	//void SetUniform(const GLchar* name, const glm::vec4& v);
	//void SetUniform(const GLchar* name, const Color& color);

private:
	// A handle to the OpenGL shader object
	GLuint glObject;
	std::vector<GLuint> shaders;

	// Link the shaders
	int link();

	// Add a shader to the GPU Program
	int addShader(GLenum shaderType, const char* name);

	GLint getUniform(const char* name) const;
};

#endif

#endif // __BUILD_WINDOWS__