#ifdef __BUILD_WINDOWS__

#include "OGL_GraphicsEngine.h"
#include "OGL_GPUProgram.h"

OGL_GraphicsEngine::OGL_GraphicsEngine()
{
}

OGL_GraphicsEngine::~OGL_GraphicsEngine()
{
	for (auto shader = shaderPrograms.begin(); shader != shaderPrograms.end(); ++shader)
	{
		if (*shader)
		{
			delete *shader;
		}
	}
}

void OGL_GraphicsEngine::initShaderPrograms()
{
	shaderPrograms.reserve(NUM_GPU_PROGRAMS);
	for (int i = 0; i < NUM_GPU_PROGRAMS; ++i)
	{
		shaderPrograms.push_back(new OGL_GPUProgram);
	}

	// manually init each shader
	//shaderPrograms[0]->init((u32*)vshader_shbin, vshader_shbin_size, 1, GPU_TEXTURE0, 0);
	//shaderPrograms[1]->init((u32*)vshader_color_shbin, vshader_color_shbin_size, 1, GPU_PRIMARY_COLOR, 1);
}

int OGL_GraphicsEngine::init()
{
}

void OGL_GraphicsEngine::setTexEnv()
{
}

void OGL_GraphicsEngine::enableDepthTest(bool test)
{
}

void OGL_GraphicsEngine::beginFrame()
{
}

void OGL_GraphicsEngine::endFrame()
{
}

void OGL_GraphicsEngine::setRenderTarget(RenderTargetType target)
{
}

void OGL_GraphicsEngine::drawSprite(float x, float y, float width, float height, int image)
{
}

void OGL_GraphicsEngine::drawTriangle(float x, float y, float width, float height)
{
}

void OGL_GraphicsEngine::drawRectangle(float x, float y, float width, float height, float* color, float alpha = 1.0f)
{
}

void OGL_GraphicsEngine::drawHUD(const PlayerSpaceCraft& player)
{
}

//void bindTexture(C3D_Tex* texture);

void OGL_GraphicsEngine::drawBackground(float* backgroundPos)
{
}

void OGL_GraphicsEngine::updateUniforms()
{
}

OGL_GPUProgram* OGL_GraphicsEngine::getShaderProgram(int index)
{
	return shaderPrograms[index];
}

#endif __BUILD_WINDOWS__