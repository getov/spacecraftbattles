#ifdef __BUILD_WINDOWS__

#ifndef __OGL_GRPAPHICS_ENGINE_H__
#define __OGL_GRPAPHICS_ENGINE_H__

#include "IGraphicsEngine.h"
#include <vector>

class PlayerSpaceCraft;
class OGL_GPUProgram;

const int NUM_GPU_PROGRAMS = 2;

class OGL_GraphicsEngine : public IGraphicsEngine
{
public:
	OGL_GraphicsEngine();
	virtual ~OGL_GraphicsEngine();

	// Do all initializations of the Graphics engine here
	// @return - error code
	virtual int init() override;

	// Set texture environment options
	// See https://www.opengl.org/sdk/docs/man2/xhtml/glTexEnv.xml
	virtual void setTexEnv() override;

	// Enable or disable depth testing
	virtual void enableDepthTest(bool test) override;

	void beginFrame();

	void endFrame();

	// Set the render target on which we should render
	void setRenderTarget(RenderTargetType target);

	void drawSprite(float x, float y, float width, float height, int image);
	void drawTriangle(float x, float y, float width, float height);
	void drawRectangle(float x, float y, float width, float height, float* color, float alpha = 1.0f);

	// Method for drawing the HUD elements
	void drawHUD(const PlayerSpaceCraft& player);

	//void bindTexture(C3D_Tex* texture);

	// Draw the animated background
	// @param backgroundPos - pointer to a 2D array with the X positions for background 1 and 2
	void drawBackground(float* backgroundPos);

	void updateUniforms();

	OGL_GPUProgram* getShaderProgram(int index);

private:
	std::vector<OGL_GPUProgram*> shaderPrograms;

	void initShaderPrograms();
};

#endif

#endif // __BUILD_WINDOWS__
