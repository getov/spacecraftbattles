#include "Player.h"

PlayerSpaceCraft::PlayerSpaceCraft()
{
}

PlayerSpaceCraft::PlayerSpaceCraft(const Vector2D& pos, const BoundingBox2D& bBox, float speed, int spriteIndex)
{
	init(pos, bBox, speed, spriteIndex);
}

void PlayerSpaceCraft::init(const Vector2D& pos, const BoundingBox2D& bBox, float speed, int spriteIndex)
{
	type = GameObjectType::player;
	position = pos;
	boundingBox = bBox;
	imageIndex = spriteIndex;
	velocity = speed;

	// defaults
	isAlive = true;
	isShieldActivated = false;
	shieldHP = 100; // set it to 100 for now. Should be 0 by default in the future
	maxHealth = 300;
	health = maxHealth;
	damage = 50;
	tileSize = Vector2D(32.0f, 32.0f);
	level = 1;
	experience = 0;
	experienceCurrentLevel = 0;
	expTotalMax = 1000;
	maxExpCurrentLvl = expTotalMax;

	dev_immortal = false;
}

void PlayerSpaceCraft::setPosition(const Vector2D& pos)
{
	position = pos;
	boundingBox.min = position;
	boundingBox.max = position + tileSize;
}

void PlayerSpaceCraft::updatePosition(const Vector2D& offset, float deltaTime)
{
	position += const_cast<Vector2D&>(offset)*deltaTime*velocity;
	boundingBox.min = position;
	boundingBox.max = position + tileSize;
}

bool PlayerSpaceCraft::isCollidingWith(const GameObject& object)
{
	return boundingBox.intersect(object.boundingBox);
}

PlayerSpaceCraft::~PlayerSpaceCraft()
{
}

void PlayerSpaceCraft::setImageIndex(int index)
{
	imageIndex = index;
}

int PlayerSpaceCraft::getImageIndex()
{
	return imageIndex;
}

void PlayerSpaceCraft::setVelocity(float speed)
{
	velocity = speed;
}

float PlayerSpaceCraft::getVelocity() const
{
	return velocity;
}

int PlayerSpaceCraft::getHP() const
{
	return health;
}

void PlayerSpaceCraft::setHP(int hp)
{
	health = hp;
}

int PlayerSpaceCraft::getMaxHP() const
{
	return maxHealth;
}

int PlayerSpaceCraft::getBaseDMG() const
{
	return damage;
}

void PlayerSpaceCraft::setBaseDMG(int dmg)
{
	damage = dmg;
}

int PlayerSpaceCraft::getShieldHP() const
{
	return shieldHP;
}

void PlayerSpaceCraft::setShieldHP(int hp)
{
	shieldHP = hp;
}

void PlayerSpaceCraft::increaseShieldHP(int hp)
{
	shieldHP += hp;
}

void PlayerSpaceCraft::takeDamage(int dmg)
{
	if (dev_immortal)
	{
		return;
	}

	if (isShieldActivated)
	{
		// ignore 30% of the damage when we have shield ON
		shieldHP -= 0.7 * dmg;

		if (shieldHP == 0)
		{
			shieldHP = 0;
			isShieldActivated = false;
		}
		else if (shieldHP < 0)
		{
			health += shieldHP;
			shieldHP = 0;
			isShieldActivated = false;
		}
	}
	else
	{
		health -= dmg;
	}

	if (health<=0)
	{
		health = 0;
		isAlive = false;
	}
}

void PlayerSpaceCraft::increaseExp(int exp)
{
	experience += exp;
	experienceCurrentLevel += exp;

	if (experience >= expTotalMax)
	{
		levelUp();
	}
}

void PlayerSpaceCraft::levelUp()
{
	++level;
	expTotalMax += expTotalMax * 2; // a value that will be tweaked along the way
	maxExpCurrentLvl = expTotalMax - maxExpCurrentLvl;

	maxHealth += 50; // a value that will be tweaked along the way

	// when leveling up, reset the current health to max
	health = maxHealth;

	// reset the current experience gained
	experienceCurrentLevel = 0;
}

int PlayerSpaceCraft::getExp() const
{
	return experience;
}

int PlayerSpaceCraft::getExpTotalMax() const
{
	return expTotalMax;
}

int PlayerSpaceCraft::getMaxExpCurrentLvl() const
{
	return maxExpCurrentLvl;
}

int PlayerSpaceCraft::getExpCurrLevel() const
{
	return experienceCurrentLevel;
}

const Vector2D& PlayerSpaceCraft::getTileSize() const
{
	return tileSize;
}

void PlayerSpaceCraft::dev_toggleImmortal()
{
	dev_immortal = !dev_immortal;
}