#ifndef __PLAYER_H__
#define __PLAYER_H__

#include "GameObject.h"

class PlayerSpaceCraft : public GameObject
{
public:
	// A flag to indicate if the SpaceCraft is alive
	bool isAlive;

	bool isShieldActivated;

	PlayerSpaceCraft();

	PlayerSpaceCraft(const Vector2D& pos, const BoundingBox2D& bBox, float speed, int spriteIndex);

	~PlayerSpaceCraft();

	void init(const Vector2D& pos, const BoundingBox2D& bBox, float speed, int spriteIndex);

	// From GameObject
	// Check for collisons with another game object
	virtual bool isCollidingWith(const GameObject& object) override;

	// Set the position and update the bounding box
	virtual void setPosition(const Vector2D& pos) override;

	// Update the position by adding an offset to the current position
	// Update the bounding box too.
	virtual void updatePosition(const Vector2D& offset, float deltaTime) override;

	void setImageIndex(int index);

	int getImageIndex();

	void setVelocity(float speed);

	float getVelocity() const;

	int getHP() const;
	void setHP(int hp);

	int getMaxHP() const;

	int getBaseDMG() const;
	void setBaseDMG(int dmg);

	int getShieldHP() const;
	void setShieldHP(int hp);
	void increaseShieldHP(int hp);

	// Should be called when the player takes damage from any source
	// It calculates the final HP and Shield numbers based on the damage taken
	void takeDamage(int dmg);

	// Increase the experience by exp amount
	void increaseExp(int exp);

	int getExp() const;

	int getMaxExpCurrentLvl() const;

	int getExpTotalMax() const;

	int getExpCurrLevel() const;

	const Vector2D& getTileSize() const;

	// For dev/testing purposes only
	void dev_toggleImmortal();

private:
	// index of the image in the sprite
	int imageIndex;

	Vector2D tileSize;

	// the movement speed of the SpaceCraft
	float velocity;

	// health
	int health;

	int maxHealth;

	// The base strength of the weapons
	int damage;

	// The HP of the shield
	int shieldHP;

	// The level of the player
	int level;

	// The total Experience points
	int experience;

	// the experience gained for the current level only
	int experienceCurrentLevel;

	// the total max experience ever
	int expTotalMax;

	// The max experience points for the current level
	int maxExpCurrentLvl;

	// Variable for test purposes only.
	// Set immortal status
	bool dev_immortal;

	// Perform the needed calculations after the Player has leveled up
	void levelUp();
};

#endif