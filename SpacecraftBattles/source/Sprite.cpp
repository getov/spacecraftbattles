#include "Sprite.h"
#include "lodepng.h"
#include "system.h"
#include <3ds.h>
#include <stdlib.h>
#include <c3d/texture.h>

Sprite::Sprite()
{
}

void Sprite::init(const u8* imageData, size_t imageDataSize)
{
	bool isTexInited = loadSprite(imageData, imageDataSize);

	if (!isTexInited)
	{
		printf("Failed to init texture\n");
	}
}

bool Sprite::loadSprite(const u8* imageData, size_t imageDataSize)
{
	u8* image;
	unsigned int width;
	unsigned int height;

	lodepng_decode32(&image, &width, &height, imageData, imageDataSize);

	// GX_DisplayTransfer needs input buffer in linear RAM
	u8* gpusrc = (u8*)linearAlloc(width*height * 4);

	// lodepng outputs big endian rgba so we need to convert
	int index = 0;
	for (int i = 0; i<width*height; i++)
	{
		gpusrc[index]     = image[index + 3]; // a
		gpusrc[index + 1] = image[index + 2]; // b
		gpusrc[index + 2] = image[index + 1]; // g
		gpusrc[index + 3] = image[index];     // r
		index += 4;
	}

	// ensure data is in physical ram
	GSPGPU_FlushDataCache(gpusrc, width*height * 4);

	// Load the texture and bind it to the first texture unit
	bool isTexInited = C3D_TexInit(&spriteTex, width, height, GPU_RGBA8);

	// Convert image to 3DS tiled texture format
	C3D_SafeDisplayTransfer((u32*)gpusrc, GX_BUFFER_DIM(width, height), (u32*)spriteTex.data, GX_BUFFER_DIM(width, height), TEXTURE_TRANSFER_FLAGS);
	gspWaitForPPF();

	free(image);
	linearFree(gpusrc);

	return isTexInited;
}