#ifndef __SPRITE_H__
#define __SPRITE_H__

#include <stdio.h>
#include <citro3d.h>

// Class representing a sprite, containing a set of images
// used for in-game elements
class Sprite
{
public:
	// A structure containing the data from the sprite image
	C3D_Tex spriteTex;

	Sprite();

	void init(const u8* imageData, size_t imageDataSize);

	bool loadSprite(const u8* imageData, size_t imageDataSize);
};

#endif
