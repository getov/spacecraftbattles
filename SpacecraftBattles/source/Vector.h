#ifndef __VECTOR_H__
#define __VECTOR_H__

#include "system.h"
#include <algorithm>
#include <cmath>

class Vector2D
{
public:
	float x;
	float y;

	Vector2D()
		: x(0.0f)
		, y(0.0f)
	{
	}

	Vector2D(float x, float y)
		: x(x)
		, y(y)
	{
	}

	void set(float x, float y)
	{
		this->x = x;
		this->y = y;
	}

	FORCEINLINE float& operator [](const int index)
	{ 
		return reinterpret_cast< float(&)[2] >(*this)[index]; 
	}

	FORCEINLINE const float& operator [](const int index) const 
	{ 
		return reinterpret_cast< const float(&)[2] >(*this)[index]; 
	}

	FORCEINLINE Vector2D operator*(float f)
	{
		x *= f;
		y *= f;
		return *this;
	}

	FORCEINLINE void operator +=(const Vector2D& a)
	{ 
		x += a.x;
		y += a.y; 
	}

	FORCEINLINE Vector2D operator+(const Vector2D& a)
	{
		return Vector2D(x + a.x, y + a.y);
	}

	// Subtracts the components of the given vector
	FORCEINLINE void operator -=(const Vector2D& a)
	{ 
		x -= a.x;
		y -= a.y;
	}

	// Multiplies all components by the given number
	FORCEINLINE void operator *=(float f)
	{ 
		x *= f; y *= f;
	}

	// Divides all components by the given number
	FORCEINLINE void operator /=(float f)
	{ 
		x /= f; y /= f;
	}

	// Reverses the sign of all components
	FORCEINLINE Vector2D operator-(void) const
	{ 
		return(Vector2D(-x, -y));
	}

	float lengthSqr() const 
	{ 
		return x*x + y*y; 
	}

	float length() const 
	{ 
		return sqrtf(x*x + y*y); 
	}

	void normalize()
	{
		float multiplier = 1.0 / length();
		x *= multiplier;
		y *= multiplier;
	}
};

FORCEINLINE Vector2D operator - (const Vector2D& a, const Vector2D& b)
{
	return Vector2D(a.x - b.x, a.y - b.y);
}

inline Vector2D vectorMin(const Vector2D& a, const Vector2D& b)
{
	return Vector2D(std::min(a.x, b.x), std::min(a.y, b.y));
}

inline Vector2D vectorMax(const Vector2D& a, const Vector2D& b)
{
	return Vector2D(std::max(a.x, b.x), std::max(a.y, b.y));
}

#endif