#include "Weapons.h"

Projectile::Projectile()
{
}

Projectile::Projectile(const Vector2D& pos, const BoundingBox2D& bBox, float speed, int spriteIndex)
{
	init(pos, bBox, speed, spriteIndex);
}

Projectile::~Projectile()
{
}

void Projectile::init(const Vector2D& pos, const BoundingBox2D& bBox, float speed, int spriteIndex)
{
	isVisible = false;
	position = pos;
	boundingBox = bBox;
	velocity = speed;
	imageIndex = spriteIndex;
	tileSize = Vector2D(16.0f, 28.0f);
}

void Projectile::setPosition(const Vector2D& pos)
{
	position = pos;
	boundingBox.min = position;
	boundingBox.max = position + tileSize;
}

void Projectile::updatePosition(const Vector2D& offset, float deltaTime)
{
	position += const_cast<Vector2D&>(offset)*deltaTime*velocity;
	boundingBox.min = position;
	boundingBox.max = position + tileSize;
}

bool Projectile::isCollidingWith(const GameObject& object)
{
	return boundingBox.intersect(object.boundingBox);;
}

float Projectile::getVelocity() const
{
	return velocity;
}

int Projectile::getImageIndex()
{
	return imageIndex;
}

void Projectile::setVelocity(float speed)
{
	velocity = speed;
}

const Vector2D& Projectile::getTileSize() const
{
	return tileSize;
}
