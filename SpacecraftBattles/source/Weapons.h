#ifndef __WEAPONS_H__
#define __WEAPONS_H__

#include "GameObject.h"

class Projectile : public GameObject
{
public:
	// Flag to indicate if we should draw the projectile and perform collisions
	bool isVisible;

	Projectile();

	Projectile(const Vector2D& pos, const BoundingBox2D& bBox, float speed, int spriteIndex);

	void init(const Vector2D& pos, const BoundingBox2D& bBox, float speed, int spriteIndex);

	~Projectile();

	// Set the position and update the bounding box
	virtual void setPosition(const Vector2D& pos) override;

	// Update the position by adding an offset to the current position
	// Update the bounding box too.
	virtual void updatePosition(const Vector2D& offset, float deltaTime) override;

	virtual bool isCollidingWith(const GameObject& object) override;

	void setVelocity(float speed);
	
	float getVelocity() const;

	int getImageIndex();

	const Vector2D& getTileSize() const;

private:
	// index of the image in the sprite
	int imageIndex;

	Vector2D tileSize;

	// The speed of the projectile
	float velocity;
};

#endif