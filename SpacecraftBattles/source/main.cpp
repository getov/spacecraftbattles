#include "GameApp.h"

int main(int argc, char **argv)
{
	GameApp& gameApp = GameApp::getInstance();
	int result = gameApp.run();

	return result;
}
