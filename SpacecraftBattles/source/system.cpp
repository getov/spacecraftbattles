#include "system.h"
#include <3ds.h>

float getSystemTimeSec()
{
	float time = 0;

#ifdef __BUILD_WINDOWS__
	// windows time
#else
	// nintendo time
	time = (float)svcGetSystemTick() / TICKS_PER_SEC;
#endif

	return time;
}