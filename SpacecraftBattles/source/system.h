#ifndef __SYSTEM_H__
#define __SYSTEM_H__

#define TICKS_PER_SEC (268123480)
const int N3DS_WIDTH = 400;
const int N3DS_HEIGHT = 240;

// FORCEINLINE definition
#if defined __INTEL_COMPILER || defined _MSC_VER
	// This is defined if the __forceinline keyword is supported by the compiler.
	#define FORCEINLINE_SUPPORTED
	// If FORCEINLINE_SUPPORTED is defined, this expands to __forceinline, and to an empty string otherwise.
	#define FORCEINLINE __forceinline
#else
	#undef FORCEINLINE
	#define FORCEINLINE __attribute__ ((always_inline)) inline
#endif

// Used to transfer the final rendered display to the framebuffer
#define DISPLAY_TRANSFER_FLAGS \
	(GX_TRANSFER_FLIP_VERT(0) | GX_TRANSFER_OUT_TILED(0) | GX_TRANSFER_RAW_COPY(0) | \
	GX_TRANSFER_IN_FORMAT(GX_TRANSFER_FMT_RGBA8) | GX_TRANSFER_OUT_FORMAT(GX_TRANSFER_FMT_RGB8) | \
	GX_TRANSFER_SCALING(GX_TRANSFER_SCALE_NO))

// Used to convert textures to 3DS tiled format
// Note: vertical flip flag set so 0,0 is top left of texture
#define TEXTURE_TRANSFER_FLAGS \
	(GX_TRANSFER_FLIP_VERT(1) | GX_TRANSFER_OUT_TILED(1) | GX_TRANSFER_RAW_COPY(0) | \
	GX_TRANSFER_IN_FORMAT(GX_TRANSFER_FMT_RGBA8) | GX_TRANSFER_OUT_FORMAT(GX_TRANSFER_FMT_RGBA8) | \
	GX_TRANSFER_SCALING(GX_TRANSFER_SCALE_NO))

// Return the system time in seconds
float getSystemTimeSec();

#endif