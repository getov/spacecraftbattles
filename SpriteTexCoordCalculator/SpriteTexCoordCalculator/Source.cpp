/*
	This program takes as an input the resolution of the texture (it is considered that width=height)
	and the tile size.
	It prints the texture coordinates [0.0 - 1.0] for the left, right, top and bottom positions of each tile in a file.
	It is considered that [0.0, 0.0] is the upper left corner
*/

#include <iostream>
#include <fstream>
#include <vector>
#include <iomanip>
using namespace std;

struct SpriteData
{
	// cube texture (e.g. 512x512)
	float resolution;
	float tileSize;
};

struct TileTextureCoords
{
	float left;
	float right;
	float top;
	float bottom;
};

SpriteData mainSpriteSheet;
TileTextureCoords tileTexCoords;

int main()
{
	cout << "Input a texture resolution: ";
	cin >> mainSpriteSheet.resolution;
	cout << "Input a tile size: ";
	cin >> mainSpriteSheet.tileSize;

	const int tilesPerRow = mainSpriteSheet.resolution / mainSpriteSheet.tileSize;
	vector<TileTextureCoords> tileTexCoordsArray(tilesPerRow*tilesPerRow);

	int index = 0;
	for (int i = 0; i < tilesPerRow; ++i)
	{
		for (int k = 0; k < tilesPerRow; ++k)
		{
			float x = k*mainSpriteSheet.tileSize;
			float y = i*mainSpriteSheet.tileSize;
			tileTexCoordsArray[index].left   = x / mainSpriteSheet.resolution;
			tileTexCoordsArray[index].right  = (x + mainSpriteSheet.tileSize) / mainSpriteSheet.resolution;
			tileTexCoordsArray[index].top    = y / mainSpriteSheet.resolution;
			tileTexCoordsArray[index].bottom = (y + mainSpriteSheet.tileSize) / mainSpriteSheet.resolution;
			++index;
		}
	}

	ofstream texCoordsOutput("texCoordsOutput.txt");
	if (texCoordsOutput.is_open())
	{
		for (auto i = tileTexCoordsArray.begin(); i != tileTexCoordsArray.end(); ++i)
		{
			texCoordsOutput << "{ " << fixed << i->left << "f, " << i->right << "f, " << i->top << "f, " << i->bottom << "f },\n";
		}
		texCoordsOutput.close();
	}

	return EXIT_SUCCESS;
}